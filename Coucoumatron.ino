#include "ULDAPI/core/vl53l1_api.c"
#include "ULDAPI/platform/vl53l1_platform.c"
#include "params.h"
#include <i2c_t3.h>
#include <Servo.h>

Servo arm;

void init_sensor(int enablePin) {
	digitalWrite(enablePin, LOW);
	delay(10);
	digitalWrite(enablePin, HIGH);

	Serial.println("Checking BootState");
	uint8_t boot_state = false, status = 0;
	while (!boot_state && !status) {
		status = VL53L1X_BootState(TOF_DEFAULT_ADDRESS, &boot_state);
		delay(2);
	}
	Serial.println("Initializing sensor");
	VL53L1X_SensorInit(TOF_DEFAULT_ADDRESS);
	Serial.println("Setting distance mode to short");
	VL53L1X_SetDistanceMode(TOF_DEFAULT_ADDRESS, 1); // 1=short, 2=long
	Serial.println("Setting viewport");
}

void coucou() {
	Serial.println("Coucou");
	digitalWrite(13, HIGH);
	arm.write(50);
	delay(1000);
	digitalWrite(13, LOW);
	arm.write(10);
}

void setup() {
	pinMode(13, OUTPUT);
	pinMode(TOF_EN_1, OUTPUT);
	pinMode(TOF_INT_1, INPUT);

	delay(1000);

	Serial.println("Start I2C");
	TOF_I2C.begin();
	
	init_sensor(TOF_EN_1);

	Serial.println("Starting ranging");
	VL53L1X_StartRanging(TOF_DEFAULT_ADDRESS);

	arm.attach(SERVO);
	arm.write(10);
}

void loop() {
	if (!digitalRead(TOF_INT_1)) {
		uint16_t distance;
		VL53L1X_GetDistance(TOF_DEFAULT_ADDRESS, &distance);
		VL53L1X_ClearInterrupt(TOF_DEFAULT_ADDRESS);

		Serial.println(distance);

		if (distance < 200) {
			coucou();
			delay(2000);
		}
	}
}
