#ifndef TOF_PARAMS
#define TOF_PARAMS 1

#define TOF_I2C Wire1

// 0x52 >> 1 = 0x29
#define TOF_DEFAULT_ADDRESS 0x29

#define TOF_EN_1 33
#define TOF_INT_1 34

#define SERVO 23

#endif
